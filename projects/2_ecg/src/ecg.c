/* (c) 2021,
 * Gustavo Di Stefano
 * gusdistefano@gmail.com
 * Argentina
 * All rights reserved.
 *
 * ____________________   ________     _________.__              .__          __
 * \_   _____/\_   ___ \ /  _____/    /   _____/|__| _____  __ __|  | _____ _/  |_  ___________
 *  |    __)_ /    \  \//   \  ___    \_____  \ |  |/     \|  |  \  | \__  \\   __\/  _ \_  __ \
 *  |        \\     \___\    \_\  \   /        \|  |  Y Y  \  |  /  |__/ __ \|  | (  <_> )  | \/
 * /_______  / \______  /\______  /  /_______  /|__|__|_|  /____/|____(____  /__|  \____/|__|
 *         \/         \/        \/           \/          \/                \/
 *
 *
 *  Simulates an electrocardiogram, starting from a data sample.
 *  This data is sent to the SerialPlot software for your typical drawing.
 *  The potentiometer increases or decreases the amplitude of the frequency.
 *  Necessary materials
 *  1- EDUCIAA
 *  2- 1K potentiometer
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*
 * Initials     Name
 * ---------------------------
 * GD		  Gustavo Di Stefano
 */

/*
 * modification history (new versions first)
 * -----------------------------------------------------------
 * 20210921 v0.0.1 commit initial version
 * 20210922 v0.0.2 fix formula signalSend line 123
 */




/*==================[inclusions]=============================================*/
#include "ecg.h"       /* <= cabecera propia */

#include "systemclock.h"
#include "timer.h"

#include "analog_io.h"
#include "uart.h"



/*==================[macros and definitions]=================================*/

/*==================[internal data definition]===============================*/

const uint8_t ECG1[]={17,17,17,17,17,17,17,17,17,17,17,18,18,18,17,17,17,17,17,17,17,18,18,18,18,18,18,18,17,17,16,16,16,16,17,17,18,18,18,17,17,17,17,
		18,18,19,21,22,24,25,26,27,28,29,31,32,33,34,34,35,37,38,37,34,29,24,19,15,14,15,16,17,17,17,16,15,14,13,13,13,13,13,13,13,12,12,
		10,6,2,3,15,43,88,145,199,237,252,242,211,167,117,70,35,16,14,22,32,38,37,32,27,24,24,26,27,28,28,27,28,28,30,31,31,31,32,33,34,36,
		38,39,40,41,42,43,45,47,49,51,53,55,57,60,62,65,68,71,75,79,83,87,92,97,101,106,111,116,121,125,129,133,136,138,139,140,140,139,137,
		133,129,123,117,109,101,92,84,77,70,64,58,52,47,42,39,36,34,31,30,28,27,26,25,25,25,25,25,25,25,25,24,24,24,24,25,25,25,25,25,25,25,
		24,24,24,24,24,24,24,24,23,23,22,22,21,21,21,20,20,20,20,20,19,19,18,18,18,19,19,19,19,18,17,17,18,18,18,18,18,18,18,18,17,17,17,17,
		17,17,17};

/*
 * configuracion de serialPlot
 * buffer 1000
 * plot with 300
 * x 0 -1500
 * y 0 - 300 (autoconfig mejor)
 * No parity
 * 8 bits
 * 1 stop bit
 * no flow control
 */


uint16_t valPot=0; //valor lectura potenciometro
uint16_t i=0; //indice para el vector

/*==================[internal functions declaration]=========================*/
void readData(void);
void processSignal(void);
void FTDI(void);
/*==================[external data definition]===============================*/

timer_config my_timer = {TIMER_A,50,&readData};
analog_input_config adc = {CH1, AINPUTS_SINGLE_READ,&processSignal};
serial_config usart = {SERIAL_PORT_PC, 115200, &FTDI};

/*==================[external functions definition]==========================*/

void readData(void)
{
	uint32_t signalSend;
	AnalogStartConvertion();

	signalSend=ECG1[i]*valPot; //valor del vector ECG1
	UartSendString(SERIAL_PORT_PC, UartItoa(signalSend, 10));
	UartSendString(SERIAL_PORT_PC, "\n");
	i++;
	i %= sizeof(ECG1);


}

void processSignal(void){
//leer el valor del potenciometro y cargar en valPot
	AnalogInputRead(CH1,&valPot);
	valPot/=102; //un numero entre 0-10 (/102 de 1 a 10)

}

void FTDI(void){
	//nothing to do
};


void SysInit(void)
{
	SystemClockInit();
	TimerInit(&my_timer);
    TimerStart(TIMER_A);
	LedsInit(); //leds OFF
	UartInit(&usart); //init USART2
	AnalogInputInit(&adc); //init ADC

}

int main(void)
{
	SysInit();

	while(1){
		//nothing to do
	}


}

/*==================[end of file]============================================*/

