
____________________   ________     _________.__              .__          __                
\_   _____/\_   ___ \ /  _____/    /   _____/|__| _____  __ __|  | _____ _/  |_  ___________ 
 |    __)_ /    \  \//   \  ___    \_____  \ |  |/     \|  |  \  | \__  \\   __\/  _ \_  __ \
 |        \\     \___\    \_\  \   /        \|  |  Y Y  \  |  /  |__/ __ \|  | (  <_> )  | \/
/_______  / \______  /\______  /  /_______  /|__|__|_|  /____/|____(____  /__|  \____/|__|   
        \/         \/        \/           \/          \/                \/                   
                                                                                             
                                                                                            
                                                                                            
Simulates an electrocardiogram, starting from a data sample.
This data is sent to the SerialPlot software for your typical drawing.
The potentiometer increases or decreases the amplitude of the frequency.
Necessary materials
1- EDUCIAA
2- 1K potentiometer                                                                          
                                                                                             
                                                                                             

