/* (c) 2021,
 * Gustavo Di Stefano
 * gusdistefano@gmail.com
 * Argentina
 * All rights reserved.
 *
 * ____________________   ________     _________.__              .__          __
 * \_   _____/\_   ___ \ /  _____/    /   _____/|__| _____  __ __|  | _____ _/  |_  ___________
 *  |    __)_ /    \  \//   \  ___    \_____  \ |  |/     \|  |  \  | \__  \\   __\/  _ \_  __ \
 *  |        \\     \___\    \_\  \   /        \|  |  Y Y  \  |  /  |__/ __ \|  | (  <_> )  | \/
 * /_______  / \______  /\______  /  /_______  /|__|__|_|  /____/|____(____  /__|  \____/|__|
 *         \/         \/        \/           \/          \/                \/
 *
 *
 *  Simulates an electrocardiogram, starting from a data sample.
 *  This data is sent to the SerialPlot software for your typical drawing.
 *  The potentiometer increases or decreases the amplitude of the frequency.
 *  Necessary materials
 *  1- EDUCIAA
 *  2- 1K potentiometer
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*
 * Initials     Name
 * ---------------------------
 * GD		  Gustavo Di Stefano
 */

/*
 * modification history (new versions first)
 * -----------------------------------------------------------
 * 20210921 v0.0.1 commit initial version
 */





#ifndef _ECG_H
#define _ECG_H


/*==================[inclusions]=============================================*/

#ifdef __cplusplus
extern "C" {
#endif

int main(void);

/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
}
#endif

/*==================[external functions declaration]=========================*/

/*==================[end of file]============================================*/


#endif /* #ifndef _ECG_H */

