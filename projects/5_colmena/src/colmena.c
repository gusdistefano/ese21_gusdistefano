
/*==================[inclusions]=============================================*/
#include "colmena.h"       /* <= cabecera propia */
#include "systemclock.h"
#include "timer.h"
#include "led.h"
#include "sim800l.h"
#include "dht22.h"
#include <string.h>



/*==================[macros and definitions]=================================*/
// La temp ideal oscila entre 32 y 36 grados
// La humedad optima de cosecha son por debajo de 20%
// la humedad optima de colmenta es del 40%
#define Msg_TMIN "Alerta: temperatura debajo del minimo: "
#define Msg_TMAX "Alerta: temperatura superior al maximo: "
#define Msg_HMIN "Aviso: humedad optima de cosecha: "  //20 %
#define Msg_HMAX "Alerta: humedad superior al maximo: " //50%
#define TMAX 36
#define TMIN 32
#define HMAX 40
#define HMIN 20
#define CELNUM "+5493492659469"
#define SENSING_TIME 1800		//en segundos


/*==================[internal data definition]===============================*/
uint16_t temp = 0;
uint16_t hum = 0;
uint8_t DHT22_Status=0; //estado del sensor de temperatura
uint8_t Sim800l_Status=0;// estado del modulo GSM

/*==================[internal functions declaration]=========================*/

/*==================[external data definition]===============================*/

sim800l sim800lCfg = {CELNUM, ""};
dht22	dht22Cfg = {6,12,2,8};


/*==================[external functions definition]==========================*/

void SysInit(void);



int main(void)
{
	char strHum[3] ={0}; //string humedad
	char strTemp[3] ={0}; //string humedad
	char str[50]={0};


	SysInit();
	while(1){



		DHT22_Status = DHT22_Read(&dht22Cfg, &hum, &temp);
		while (DHT22_Status==2){ //error de Paridad --> Reeler el sensor
			DelaySec(5);//espero 5s antes de otra lectura
			DHT22_Status = DHT22_Read(&dht22Cfg, &hum, &temp);
		}
		DHT22_Status = 1;
		if (DHT22_Status==1){	//sensor presente y lectura correcta
				//el driver devuelve un numero de tres cifras entero,
						//donde el tercer digito es el decimal
						hum/=10;
						temp/=10;
						strcat(strHum,UartItoa(hum,10));
						strcat(strTemp, UartItoa(temp,10));

						//envio de alertas cada media hora
						if (temp>TMAX) {
								strcat(str, Msg_TMAX);
								strcat(str, strTemp);
								strcat(str,"\r\n");
								sim800lCfg.msg = str;
								Sim800l_SendMsg(&sim800lCfg);
								memset(str,0,50);
								}
						if (temp<TMIN) {
								strcat(str, Msg_TMIN);
								strcat(str, strTemp);
								strcat(str,"\r\n");
								sim800lCfg.msg = str;
								Sim800l_SendMsg(&sim800lCfg);
								memset(str,0,50);
								}

						if (hum>HMAX) {
								strcat(str, Msg_HMAX);
								strcat(str, strHum);
								strcat(str,"\r\n");
								sim800lCfg.msg = str;
								Sim800l_SendMsg(&sim800lCfg);
								memset(str,0,50);
								}
						if (hum<HMIN) {
								strcat(str, Msg_HMIN);
								strcat(str, strHum);
								strcat(str,"\r\n");
								sim800lCfg.msg = str;
								Sim800l_SendMsg(&sim800lCfg);
								memset(str,0,50);
								}

			} //fin DTH22_Status==1
		if (DHT22_Status==0) LedOn(LED_1);//enciende led avisando que no se detecto sensor


		DelaySec(SENSING_TIME); 		//pausa de 30 min, antes de hacer otra lectura

	};


}

/*==================[implementation of functions]============================================*/

void SysInit(void)
{
	SystemClockInit();
	LedsInit(); //leds OFF
	DHT22_Status= DHT22_Init(&dht22Cfg); //inicializar sensor DHT22
	while (!DHT22_Status){
		LedOn(LED_1);//enciende led avisando que hay error
		DelaySec(5); //espero 5 seg y reintento
		DHT22_Status= DHT22_Init(&dht22Cfg);
	}
	LedOff(LED_1);

	//INICIALIZAR EL MODULO GSM
	Sim800l_Status = Sim800l_Init(&sim800lCfg);// devuelve 0 si no detecto / 1 operacion normal
	while (!Sim800l_Status){
		LedOn(LED_2);//encender led rojo, indicando que no responde el modulo
		Sim800l_Status=Sim800l_Test();
		DelaySec(120);//espero 2 min y reintento

	}
	LedOff(LED_2);
}



/*==================[end of file]============================================*/

