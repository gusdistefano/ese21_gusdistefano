/* (c) 2021,
 * Gustavo Di Stefano
 * gusdistefano@gmail.com
 * Argentina
 * All rights reserved.
 *       _________                __                .__
 *       \_   ___ \  ____   _____/  |________  ____ |  |
 *       /    \  \/ /  _ \ /    \   __\_  __ \/  _ \|  |
 *       \     \___(  <_> )   |  \  |  |  | \(  <_> )  |__
 *        \______  /\____/|___|  /__|  |__|   \____/|____/
 *               \/            \/
 *     .___       _________        .__
 *   __| _/____   \_   ___ \  ____ |  |   _____   ____   ____ _____
 *  / __ |/ __ \  /    \  \/ /  _ \|  |  /     \_/ __ \ /    \\__  \
 * / /_/ \  ___/  \     \___(  <_> )  |_|  Y Y  \  ___/|   |  \/ __ \_
 * \____ |\___  >  \______  /\____/|____/__|_|  /\___  >___|  (____  /
 *      \/    \/          \/                  \/     \/     \/     \/
 *
 *
 * The Hive Control system monitors the temperature and humidity parameters
 * of a hive. Send an SMS to the beekeeper in case any temperature or humidity value
 * is not within the established ranges.
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*
 * Initials     Name
 * ---------------------------
 * GD		  Gustavo Di Stefano
 */

/*
 * modification history (new versions first)
 * -----------------------------------------------------------
 * 20211027 v0.0.1 commit initial version
 */





#ifndef _COLMENA_H
#define _COLMENA_H


/*==================[inclusions]=============================================*/

#ifdef __cplusplus
extern "C" {
#endif

int main(void);

/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
}
#endif

/*==================[external functions declaration]=========================*/

/*==================[end of file]============================================*/


#endif /* #ifndef _COLMENA_H */

