

/*==================[inclusions]=============================================*/

#include "dht22.h"
#include "uart.h"
#include "systemclock.h"



/*==================[macros and definitions]=================================*/

/*==================[internal data definition]===============================*/



/*==================[internal functions declaration]=========================*/
/*==================[external data definition]===============================*/
void FTDI(void){
	//nothing to do
};


dht22 sensor = {6,12,2,8}; //GPIO8 SCU 6-12  //  gpioPort-pin 2,8
serial_config usart = {SERIAL_PORT_PC, 115200, &FTDI};

/*==================[external functions definition]==========================*/









void SysInit(void)
{
	SystemClockInit();
	UartInit(&usart); //init USART2


}

int main(void)
{
	SysInit();
	int a;
	uint16_t hum, temp;
	a=DHT22_Init(&sensor);
	DHT22_Read(&sensor, &hum, &temp);



return 0;

}

/*==================[end of file]============================================*/

