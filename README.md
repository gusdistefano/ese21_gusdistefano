# Título del Proyecto

El presente trabajo, pretende, mediante el desarrollo de una aplicación sobre microcontroladores y dispositivos asociados, la monitorización a distancia de los valores de temperatura y humedad de la colmena.

	Los parámetros de temperatura, humedad y peso son tres factores determinantes para el desarrollo de una colmena. La información que aportan estos parámetros tiene relevancia para la toma de decisiones acerca de:

        ◦ momento de recolección
        ◦ detección temprana de posibles enfermedades debido al aumento de la humedad
        ◦ intoxicación por pesticidas
        ◦ prevención del enjambrazon
        ◦ necesidad de alimentar la colmena
  
	Con la lectura a distancia de estos parámetros, permitiría mejorar la productividad y rentabilidad del apicultor, como también la reducción de tiempos en el manejo de colmena y disminución de la mortalidad. 

	Esta información aportada permitirá comprender cuales son los factores de riesgo relacionados con el cambio climático, la actividad agropecuaria y el uso de agroquímicos, evitando a futuro la mortalidad de abejas.

	Este tipo de sistemas, no sustituye las revisiones presenciales, pero permiten establecer prioridades tras el análisis de los datos.



## Autor ✒️


* **Gustavo Di Stefano** - *Trabajo Inicial* -(https://gitlab.com/gusdistefano)


## Licencia 📄

Este proyecto está bajo la Licencia (GNU GPL) - [LICENSE.md](LICENSE.md) para detalles

## Agradecimientos 🎁

* Gracias a la catedra por iniciarnos en esta actividad, a Eduardo y Juan 📢
* A fin de año espero poder juntarnos 🍺 


---



