/* Copyright 2016, 
 * Leandro D. Medus
 * lmedus@bioingenieria.edu.ar
 * Eduardo Filomena
 * efilomena@bioingenieria.edu.ar
 * Juan Manuel Reta
 * jmrera@bioingenieria.edu.ar
 * Facultad de Ingeniería
 * Universidad Nacional de Entre Ríos
 * Argentina
 * Sebastián Mateos
 * sebastianantoniomateos@gmail.com
 * Gustavo Di Stefano
 * gusdistefano@gmail.com
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */


#ifndef UART_H
#define UART_H

/** \brief UART Bare Metal driver for the peripheral in the EDU-CIAA Board.
 **
 ** This is a driver to control the UART present in the EDU-CIAA Board.
 **
 **/

/** \addtogroup Drivers_Programable Drivers Programable
 ** @{ */
/** \addtogroup Drivers_Microcontroller Drivers microcontroller
 ** @{ */
/** \addtogroup UART UART
 ** @{ */

/*
 * Initials     Name
 * ---------------------------
 *	LM			Leandro Medus
 *	GD			Gustavo Di Stefano
 */

/*
 * modification history (new versions first)
 * -----------------------------------------------------------
 * 20211010 v0.2 GD  cambio de nombre a funciones
 *	 				 UartReadStatus --> UartTxReady Verifica si esta listo a trasnmistir
 * 					 Se agregan comentarios y descripciones de uso
 *
 * 20160610 v0.1 initials initial version leo

 *
 */

///////////////// REGISTROS LSR DE LA UART  ///////////////////////////////////
/*#define UART_LSR_RDR        (1 << 0)	< Line status: Receive data ready
#define UART_LSR_OE         (1 << 1)	< Line status: Overrun error
#define UART_LSR_PE         (1 << 2)	< Line status: Parity error
#define UART_LSR_FE         (1 << 3)	< Line status: Framing error
#define UART_LSR_BI         (1 << 4)	< Line status: Break interrupt
#define UART_LSR_THRE       (1 << 5)	< Line status: Transmit holding register empty
#define UART_LSR_TEMT       (1 << 6)	< Line status: Transmitter empty
#define UART_LSR_RXFE       (1 << 7)	< Line status: Error in RX FIFO
#define UART_LSR_TXFE       (1 << 8)	< Line status: Error in RX FIFO
#define UART_LSR_BITMASK    (0xFF)		< UART Line status bit mask
#define UART1_LSR_BITMASK   (0x1FF)		< UART1 Line status bit mask - valid for 11xx, 18xx/43xx UART0/2/3 only

Enviar caracteres
Antes de enviar tenemos que controlar el bit 'THR Empty'
(THR es el registro de almacenamiento del transmisor,
que tiene que estar vacio antes de poder recibir un valor nuevo).
Si THR Empty esta en 1, entonces podemos enviar el caracter siguiente.

El registro THR (transmit Holding Register) almacena el byte a transmitir

Recibir caracteres
Los datos recibidos del modulo deben ser leidos, pero no leerlos demasiado rapido!
Nuevamente, un bit en el mismo registro (LSR) nos indica cuando es el momento correcto:
'Data Ready' nos marca cuando llego un valor valido nuevo.
Si el bit 0 del registro LSR esta en 1 indica que hay un dato disponible
para leer en RHR (Receiver Holding Register)

*/
/*==================[inclusions]=============================================*/
#include "stdint.h"
#include "chip.h"

/*==================[macros]=================================================*/

/*==================[typedef]================================================*/

#define SERIAL_PORT_PC				0
#define SERIAL_PORT_P2_CONNECTOR	1
#define SERIAL_PORT_RS485			2

#define NO_INT	0		/*NULL pointer for no interrupt port handler*/

typedef struct {				/*!< Serial Ports Struct*/
		uint8_t port;			/*!< port: FTDI, RS232, RS485*/
		uint32_t baud_rate;		/*!< Baud Rate*/
		void *pSerial;			/*!< Function pointer to app serial port*/
} serial_config;


/*==================[external data declaration]==============================*/

/*==================[external functions declaration]=========================*/

/** @fn uint32_t UartInit(serial_config *port)
 * @brief Inicializar UART
 * @param[in] port Puerto para inicializar (FTDI, RS232, RS485)
 */
uint32_t UartInit(serial_config *port);



/** @fn uint32_t UartTxReady(uint8_t port)
 * @brief Lee en el registro LSR (Line Control Register) de la UART
 * el bit nro 5 THR Empty (Transmit holding register). Si esta vacio se puede enviar un
 * caracter para transmitir
 * @param[in] port Puerto que se desea leer
 */
uint32_t UartTxReady(uint8_t port);



/** @fn uint32_t UartRxReady(uint8_t port)
 * @brief Consulta si hay un byte disponible mediante el LSR (Line Status Register) de
 * la UART. Consulta en LSR por el bit 0, DATA READY Receive data ready .
 * Si esta disponible luego se procede a leer el RHR (Receive Holding Register)
 * @param[in] port Puerto que se desea leer
 */
uint32_t UartRxReady(uint8_t port);



/** @fn uint8_t UartReadByte(uint8_t port, uint8_t* dat)
 * @brief Lee un byte del registro RHR (Receiver Holding Register),
 * previa consulta al bit 0 del LSR (Data Ready) mediante el uso de la funcion
 * UartRxReady, que nos indica que hay un dato para ser leido
 * @param[in] port Puerto que se desea leer
 */
uint8_t UartReadByte(uint8_t port, uint8_t* dat);



void UartSendByte(uint8_t port,uint8_t* dat);
void UartSendString(uint8_t port, uint8_t *msg);/*Stops on '\0'*/
void UartSendBuffer(uint8_t port, const void *data, uint8_t nbytes);  /*Send n Bytes - Inline candidate...*/

/** @fn char* UartItoa(uint32_t val, uint8_t base)
 * @brief Conversor de entero a ASCII
 * @param[in] val Valor entero que se desea convertir
 * @param[in] base Base sobre la cual se desea realizar la conversion
 * @return Puntero al primer elemento de la cadena convertida
 */
uint8_t* UartItoa(uint32_t val, uint8_t base);

/** @} doxygen end group definition */
/** @} doxygen end group definition */
/** @} doxygen end group definition */
/*==================[end of file]============================================*/
#endif /* #ifndef UART_H */

