#include "sim800l.h"
#include "delay.h"
#include "uart.h"
#include "string.h"

uint8_t dataFlag=0; /*!< bandera de llegada de datos*/
//procesamiento de la interrupcion por llegada de datos
void Sim800l_Read(void){
	int i=0; //contador
	uint8_t dataRHR; //UART - byte en RHR (Receivier Holding Register)
	while(UartReadByte(SERIAL_PORT_P2_CONNECTOR, &dataRHR)){
		dataRx[i]=dataRHR;
		if (i>=50) i=0; //reseteo i y sigo leyendo
		i++;
		DelayMs(10);//retardo para completar la lectura
	}
	dataFlag =1; //informo que hay datos disponibles en dataRx[]

}
serial_config serialCfg = {SERIAL_PORT_P2_CONNECTOR, 9600, &Sim800l_Read};/*!< Serial Port, baud rate, interrupt function*/


/* inicializa el modulo. Si el modulo esta presente y se activo sin problemas devuelve 1,
   sino devuelve 0*/
uint8_t Sim800l_Init(sim800l *conf){
	uint8_t ret=0;
	UartInit(&serialCfg); //init USART3 Serial Port en P2
	ret=Sim800l_Test();; //devuelve 1 si esta OK, cero en otro caso
	//configurar modo texto el modulo SIM800
	if (ret==1){ //si el modulo responde termino de configurar
				memset(dataRx,0,50);
				UartSendString(SERIAL_PORT_P2_CONNECTOR, "AT+CMGF=1\r\n");
				DelaySec(1);//espera 1 segundo antes del proximo comando
				dataFlag=0;
				}
	return ret;

}

// testea que este activo y listo para comunicaciones
uint8_t Sim800l_Test(void){
	uint8_t retry=0; //reintentos en la espera
	//envia comando y espera que el modulo responda con OK.
	dataFlag=0; //limpio bandera de lectura
	UartSendString(SERIAL_PORT_P2_CONNECTOR, "AT\r\n");
	while (!dataFlag && retry<5){
		DelaySec(1);
		retry++;
	}
	dataFlag=0; //limpio bandera de lectura
	if ((retry>=5) || (strcmp(dataRx, SIM_OK)!=0))  return 0;	//modulo no responde o no esta listo
	return 1; //modulo listo
}

// envia un mensaje de texto con la configuracion de los parametros

void Sim800l_SendMsg(sim800l *conf){

	char ctrlZ=26;	//caracter ASCII Crtl Z
	char str[40]={0};
	//configurar numero de celular
	strcat(str,"AT+CMGS=\"");//el nro de celular entre comillas
	strcat(str,conf->number);
	strcat(str,"\"\r\n");
	memset(dataRx,0,50);
	//envio cod AT con nro destino
	UartSendString(SERIAL_PORT_P2_CONNECTOR, str);
	//envio de mensaje y finalizo con CTRL Z
	UartSendString(SERIAL_PORT_P2_CONNECTOR, conf->msg);
	DelaySec(1);
	UartSendByte(SERIAL_PORT_P2_CONNECTOR, &ctrlZ);

}
/* Comandos AT principales
("AT"); //Verifica que el modulo responda con OK.
("ATI"); //Muestra la versión del modulo.
("AT+CSQ"); // Calidad de la señal, 0-31.
("AT+CBC"); // Regresa el status de la batería, % y voltaje.
("AT+CMGF=1 "); //Activa el modo sms.
*/
