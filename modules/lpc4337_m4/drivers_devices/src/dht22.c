/* Copyright 2021,
 * Gustavo Di Stefano
 * gusdistefano@gmail.com
 * Especializacion en Sistemas Embebidos
 * Facultad de Ingeniería
 * Universidad Nacional de Entre Ríos
 * Argentina
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */



/** @brief Bare Metal driver for DHT22 Temperature and humidity module for EDU-CIAA NXP
 **
 ** This driver is for the DHT22 module, temperature and humidity sensor
 **
 **/

/*
 * Initials     Name
 * ---------------------------
 *  GD		Gustavo Di Stefano
 */

/*
 * modification history (new versions first)
 * -----------------------------------------------------------
 * 20210922 v0.1 initials initial version
 */

/** @file dht22.c
 *  @brief DHT22 library
 *
 */



/*==================[inclusions]=============================================*/
#include "chip.h"
#include "dht22.h"
#include "delay.h"


//--------------------------------------------------------
/*==================[macros and definitions]=================================*/

//las siguientes macros tienen como objetivo simplicar el codigo y mejorar la legibilidad

#define S_LOW Chip_GPIO_SetPinOutLow 	(LPC_GPIO_PORT, dht_conf->gpioPort, dht_conf->gpioPin) //PortGPIO LowState
#define S_HIGH Chip_GPIO_SetPinOutHigh(LPC_GPIO_PORT, dht_conf->gpioPort, dht_conf->gpioPin) //PortGPIO HighState

#define OUT_MODE Chip_GPIO_SetPinDIR (LPC_GPIO_PORT, dht_conf->gpioPort, dht_conf->gpioPin, 1) //PortGPIO mode OUTPUT
#define IN_MODE  Chip_GPIO_SetPinDIR (LPC_GPIO_PORT, dht_conf->gpioPort, dht_conf->gpioPin, 0) //PortGPIO mode INPUT

#define IN_Clear Chip_GPIO_ClearValue(LPC_GPIO_PORT, dht_conf->gpioPort, dht_conf->gpioPin) //poner en Low solo en modo INPUT

#define READ Chip_GPIO_GetPinState(LPC_GPIO_PORT, dht_conf->gpioPort, dht_conf->gpioPin) //TRUE si High - FALSE is Low

#define SCU_PIN Chip_SCU_PinMux(dht_conf->scuPort,dht_conf->scuPin, SCU_MODE_PULLUP|SCU_MODE_INBUFF_EN|SCU_MODE_ZIF_DIS,FUNC0)
#define GPIO_INIT Chip_GPIO_Init(LPC_GPIO_PORT)
/*==================[external functions definition]==========================*/

//------------------------------------------------------------
//------------------------------------------------------------
//------------------------------------------------------------
//inicializa y comprueba la existencia del sensor
// devuelve 1 si es correcto 0 otro caso
uint8_t DHT22_Init(dht22 *dht_conf){
	int retry=0;
	SCU_PIN; //setear SCU
	GPIO_INIT; //inicializar GPIO
	OUT_MODE; //GPIO modo Output
	S_LOW;
	DelayMs(10); //maximo tiempo abajo (1-10 ms)
	//ahora la subo
	S_HIGH;
	DelayUs(30); //20-40uS
	//modo lectura
	IN_MODE;
	while(!READ && retry<100){//mientras este abajo
			DelayUs(1);
			retry++;
		}
	if (retry>=100) return 0; //no se detecto
	retry=0;
	while(READ && retry<100){//mientras este abajo
			DelayUs(1);
			retry++;
	}
	if (retry>=100) return 0; //no se detecto
	return 1; //exito

}

//Lee el sensor, calcula el checksum
// devuelve 1 si lectura correcta
// 0 no se detecto sensor
// 2 error de lectura

uint8_t DHT22_Read(dht22 *dht_conf, uint16_t *hum, uint16_t *temp){
	int i=0;
	int j=0;
	uint8_t data[5]; //cinco bytes de datos del sensor

	uint8_t retry=0; //cntador de intentos para detectar sensor
	uint8_t timeSignalLow; //tiempo en us de señal baja

	OUT_MODE; //colocar modo output para bajar señal
	S_LOW; 	//ahora la bajo
	DelayMs(10); //maximo tiempo abajo (1-10 ms)
	S_HIGH; //ahora la subo //20-40uS
	DelayUs(30);

	//sensor comienza transmision -- modo lectura
	IN_MODE;

	//se recibe bajo y alto (40-80us) luego comienzan los bits
	while(!READ && retry<100){//mientras este abajo
			DelayUs(1);
			retry++;
			}
	if (retry>=100) return 0; //no se detecto
	retry=0;

	while(READ && retry<100){//mientras este abajo
			DelayUs(1);
			retry++;
		}
	if (retry>=100) return 0; //no se detecto

//////////////////////////// empieza transmision de bits
// para detectar ceros y unos se guarda el tiempo de la señal en bajo
// si la señal alta es menor que el tiempo abajo es CERO
// si la señal alta es MAYOR que el tiempo abajo es UNO
	while(j<40){  //lee 40 bits
		i=0;
		while(!READ && i<100){//mientras este abajo
			DelayUs(1);
			i++;
			}
		timeSignalLow=i;  //guardar tiempo señal en bajo
		i=0;
		while(READ && i<100){//mientras este arriba
			DelayUs(1);
			i++;
			}

/////////cargar el dato bit a bit en 5 bytes ////////////////////////
		data[j/8]<<=1;  //desplazar un bit a izq
		if (i>timeSignalLow) data[j/8]|=1; 	 // si la señal alta es MAYOR que el tiempo abajo es UNO
							//si la señal alta es menor que el tiempo abajo es CERO

/////////////////////////////////////////////////////////////////
		j++;
	}

// checksum
	if  (data[0]+data[1]+data[2]+data[3]==data[4]) {
				*hum=data[0]*100+data[1];
				*temp=data[2]*100+data[3];
	}
	else return 2; //error de lectura


	return 1;
}


























//esta funcion toma el muestreo de datos recibidos del sensor
// solo se usa para poner a punto el sensor
uint8_t prueba(dht22 *dht_conf, uint16_t *hum, uint16_t *temp){
	int i=0;
	int j=0;
	uint8_t data[5]; //cinco bytes de datos del sensor
	uint8_t muestra[85];
	memset(muestra,0,85);
	uint8_t tmp;
	uint8_t retry=0; //cntador de intentos para detectar sensor
	uint8_t timeSignalLow; //tiempo en us de señal baja

	OUT_MODE; //colocar modo output para bajar señal
	S_LOW; 	//ahora la bajo
	DelayMs(10); //maximo tiempo abajo (1-10 ms)
	S_HIGH; //ahora la subo //20-40uS
	DelayUs(30);

	//sensor comienza transmision -- modo lectura
	IN_MODE;

	//se recibe bajo y alto (40-80us) luego comienzan los bits
	while(!READ && retry<100){//mientras este abajo
			DelayUs(1);
			retry++;
			}
	if (retry>=100) return 0; //no se detecto
	muestra[0]=retry;
	retry=0;


	while(READ && retry<100){//mientras este abajo
			DelayUs(1);
			retry++;
		}
	if (retry>=100) return 0; //no se detecto
	muestra[1]=retry;
	tmp=2;
//////////////////////////// empieza transmision de bits
// para detectar ceros y unos se guarda el tiempo de la señal en bajo
// si la señal alta es menor que el tiempo abajo es CERO
// si la señal alta es MAYOR que el tiempo abajo es UNO
	while(j<42){  //lee 40 bits

		while(!READ && i<100){//mientras este abajo
			DelayUs(1);
			i++;
			}
		timeSignalLow=i;  //guardar tiempo señal en bajo
		muestra[tmp]=i;
		tmp++;
		i=0;

		while(READ && i<100){//mientras este arriba
			DelayUs(1);
			i++;
			}
		muestra[tmp]=i;
		tmp++;
		i=0;
/////////cargar el dato bit a bit en 5 bytes ////////////////////////

/////////////////////////////////////////////////////////////////
		j++;
	}

	DelayMs(1);

	return 1;
}


