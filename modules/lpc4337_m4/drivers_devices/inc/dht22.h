/* Copyright 2021,
 * Gustavo Di Stefano
 * gusdistefano@gmail.com
 * Especializacion en Sistemas Embebidos
 * Facultad de Ingeniería
 * Universidad Nacional de Entre Ríos
 * Argentina
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */


#ifndef DHT22_H_
#define DHT22_H_

/** \addtogroup Drivers_Programable Drivers Programable
 ** @{ */
/** \addtogroup Drivers_Devices Drivers devices
 ** @{ */
/** \addtogroup DHT22_Sensor DHT22 Sensor Temperature and humidity
 ** @brief This driver is for the DHT22 module, temperature and humidity sensor
 ** @{ */


/*
 * Initials     Name
 * ---------------------------
 *  GD		Gustavo Di Stefano
 */

/*
 * modification history (new versions first)
 * -----------------------------------------------------------
 * 20210922 v0.1 initials initial version
 */



/*==================[inclusions]=============================================*/
#include "stdint.h"

/*==================[macros]=================================================*/
/*==================[typedef]================================================*/
/** @brief structure to configure the sensor, where the SCU and GPIO pins are loaded
** example scuPort=6, scuPin=12, gpioPort=2, gpioPin=8
 **/
typedef struct {			/*!< Sensor Struct*/
		uint8_t scuPort;	/*!< scu port 6 - GPIO8*/
		uint8_t scuPin;		/*!< scu pin 12 - GPIO8*/
		uint8_t gpioPort;	/*!< gpioPort 2*/
		uint8_t gpioPin;	/*!< gpioPin 8*/
} dht22;


/*==================[external data declaration]==============================*/

/*==================[external functions declaration]=========================*/
/** @fn uint8_t DHT22_Read(dht22 *dht_conf, uint16_t *hum, uint16_t *temp)
 * @brief initializes the sensor with the parameters and detects the existence
 * Returns 1 if present, 0 if not
 * @param[in] dht22 *dht_conf sensor configuration
 * @return  0 if the sensor is not detected
 * 			1 sensor detected
 */
uint8_t DHT22_Init(dht22 *dht_conf);

/** @fn uint8_t DHT22_Read(dht22 *dht_conf, uint16_t *hum, uint16_t *temp)
 * @brief It reads the sensor, returning the humidity and temperature values.
 * It first detects if the sensor is present, then takes the reading.
 * Before returning the value it executes a checksum.
 * If it is not correct it returns the value 2.
 * If everything has gone well it returns 1 and the humidity and temperature values
 * @param[in] dht22 *dht_conf sensor configuration
 * @param[out] uint16_t *hum  humidity value
 * @param[out] uint16_t *temp  temperature value
 * @return  0 if the sensor is not detected
 * 			1 correct reading
 * 			2 read error
 */
uint8_t DHT22_Read(dht22 *dht_conf, uint16_t *hum, uint16_t *temp);


/** @} doxygen end group definition */
/** @} doxygen end group definition */
/** @} doxygen end group definition */
/*==================[end of file]============================================*/
#endif /* #ifndef DHT22_H_ */


















