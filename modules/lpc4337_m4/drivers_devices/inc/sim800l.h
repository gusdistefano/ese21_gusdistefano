/* Copyright 2021,
 * Gustavo Di Stefano
 * gusdistefano@gmail.com
 * Especializacion en Sistemas Embebidos
 * Facultad de Ingeniería
 * Universidad Nacional de Entre Ríos
 * Argentina
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */


#ifndef SIM800L_H_
#define SIM800L_H_

/** \addtogroup Drivers_Programable Drivers Programable
 ** @{ */
/** \addtogroup Drivers_Devices Drivers devices
 ** @{ */
/** \addtogroup SIM800L SIM800L GSM/GPRS Module quad-band
 ** @brief The board is based on the SIM800L GSM module from SIMCOM.
 ** This module provides a complete 850/900/1800/1900MHz quad-band
 ** GSM/GPRS solution with worldwide coverage, suitable for all types of applications:
 ** remote monitoring and control, robotics, autonomous vehicles, object or vehicle tracking, etc.
 ** It is a product of 2.8V logic levels therefore it is necessary to adapt the levels when used with 3.3V and 5V boards.
 ** The module is configured and controlled through a serial connection (UART) and using AT commands.
 **
 ** @{ */


/*
 * Initials     Name
 * ---------------------------
 *  GD		Gustavo Di Stefano
 */

/*
 * modification history (new versions first)
 * -----------------------------------------------------------
 * 20210922 v0.1 initials initial version
 */


/*==================[inclusions]=============================================*/
#include "stdint.h"



/*==================[macros and definitions]=================================================*/
#define SIM_ERROR "\r\nERROR\r\n"
#define SIM_OK "\r\nOK\r\n"
/*==================[typedef]================================================*/

/** @brief structure to configure the SIM800L module connected to RS232.
 * You need to define the cell phone number and the message to transmit
 **/

typedef struct {			/*!< Module SIM800L Struct*/
		char *number;		/*!< phone number*/
		char *msg;			/*!< message to transmit*/
} sim800l;



/*==================[external data declaration]==============================*/
char dataRx[50]; /*!< vector de lectura de datos*/
/*==================[external functions declaration]=========================*/

/** @fn uint8_t Sim800l_Init(sim800l *conf)
 * @brief initializes the sensor with the parameters and detects the existence
 * Returns 1 if present, 0 if not
 * @param[in] sim800l *conf module configuration
 * @return  0 if the sensor is not detected
 * 			1 sensor detected
 */
uint8_t Sim800l_Init(sim800l *conf);


/** @fn void Sim800l_SendMsg(sim800l *conf)
 * @brief send a text message with the parameter settings
 * @param[in] sim800l *conf module configuration
 * @return  void
 */
void Sim800l_SendMsg(sim800l *conf);

// testea que este activo y listo para comunicaciones

/** @fn uint8_t Sim800l_Test(void)
 * @brief test that it is active and ready for communications
 * Returns 1 if OK, 0 if not
 * @return  0 module does not respond
 * 			1 module is ready
 */
uint8_t Sim800l_Test(void);

//Funcion para leer durante la interrupcion de la USART los bytes del buffer
//USART3 en P2 es el RS232


/** @fn void Sim800l_Read(void)
 * @brief Function to read the bytes from the buffer during the USART interrupt
 */
void Sim800l_Read(void); //funcion de la interrupcion





/** @} doxygen end group definition */
/** @} doxygen end group definition */
/** @} doxygen end group definition */
/*==================[end of file]============================================*/
#endif /* #ifndef SIM800L_H_ */










