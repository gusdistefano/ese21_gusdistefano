var group___s_i_m800_l =
[
    [ "sim800l", "structsim800l.html", [
      [ "msg", "structsim800l.html#a32d2f5216cddb59c7cc8fb2806a7e727", null ],
      [ "number", "structsim800l.html#a77370bcefb3fe21e5f84e230581a50d7", null ]
    ] ],
    [ "SIM_ERROR", "group___s_i_m800_l.html#gad33d1a9a1caf6548cdf5bea2f6fafda4", null ],
    [ "SIM_OK", "group___s_i_m800_l.html#ga83c243bc4569de1b478ecc6a8ae2b4bb", null ],
    [ "Sim800l_Init", "group___s_i_m800_l.html#ga574b28c4c71a589dec109520ed01acaf", null ],
    [ "Sim800l_Read", "group___s_i_m800_l.html#ga80f2d64af0f3287c05e8e1d9d12d08bd", null ],
    [ "Sim800l_SendMsg", "group___s_i_m800_l.html#gab927a4dc5ec5a54aa23b9df266450c28", null ],
    [ "Sim800l_Test", "group___s_i_m800_l.html#ga729b46a61f272257216e5fc2a4188a10", null ],
    [ "dataRx", "group___s_i_m800_l.html#gab5453a6c74cecda20164b59217bda037", null ]
];