var group___u_s_b_d___c_d_c =
[
    [ "USBD_CDC_INIT_PARAM", "struct_u_s_b_d___c_d_c___i_n_i_t___p_a_r_a_m.html", [
      [ "CDC_BulkIN_Hdlr", "struct_u_s_b_d___c_d_c___i_n_i_t___p_a_r_a_m.html#a2bbc43b47705e00c84b79fd44953a2f1", null ],
      [ "CDC_BulkOUT_Hdlr", "struct_u_s_b_d___c_d_c___i_n_i_t___p_a_r_a_m.html#a6d05cde0feaadf0af5a01befc1acf17c", null ],
      [ "CDC_Ep0_Hdlr", "struct_u_s_b_d___c_d_c___i_n_i_t___p_a_r_a_m.html#a165014df7a21a006a2eda6632c583137", null ],
      [ "CDC_InterruptEP_Hdlr", "struct_u_s_b_d___c_d_c___i_n_i_t___p_a_r_a_m.html#a159b785288ac361accf2a55079e99e3d", null ],
      [ "CIC_GetRequest", "struct_u_s_b_d___c_d_c___i_n_i_t___p_a_r_a_m.html#a41e3b510f82d26624040eac4d1a53ed8", null ],
      [ "CIC_SetRequest", "struct_u_s_b_d___c_d_c___i_n_i_t___p_a_r_a_m.html#aeae7e7a45a95176f1a0422a807f34ef0", null ],
      [ "cif_intf_desc", "struct_u_s_b_d___c_d_c___i_n_i_t___p_a_r_a_m.html#a2273cb833eb2d825c565fa61511bd935", null ],
      [ "ClrCommFeature", "struct_u_s_b_d___c_d_c___i_n_i_t___p_a_r_a_m.html#a0fe014e6ee1c51e2bf8a03f54a444a4c", null ],
      [ "dif_intf_desc", "struct_u_s_b_d___c_d_c___i_n_i_t___p_a_r_a_m.html#a3388a78b025fde02b2bc272bd2d6df40", null ],
      [ "GetCommFeature", "struct_u_s_b_d___c_d_c___i_n_i_t___p_a_r_a_m.html#a9c72131b3a5344fad61f36d7660aa903", null ],
      [ "GetEncpsResp", "struct_u_s_b_d___c_d_c___i_n_i_t___p_a_r_a_m.html#af7ef7cf9abc4b13bf60abd1b57f21fc3", null ],
      [ "mem_base", "struct_u_s_b_d___c_d_c___i_n_i_t___p_a_r_a_m.html#a5ba2f6f78705b63f0788f9b85f7f104c", null ],
      [ "mem_size", "struct_u_s_b_d___c_d_c___i_n_i_t___p_a_r_a_m.html#a3f6a3b0b58cbdacf4ce9a0c9b3d64794", null ],
      [ "SendBreak", "struct_u_s_b_d___c_d_c___i_n_i_t___p_a_r_a_m.html#a5663bec95ba583cab4e2c4b722267467", null ],
      [ "SendEncpsCmd", "struct_u_s_b_d___c_d_c___i_n_i_t___p_a_r_a_m.html#a8941f450aebeb6891fab3f456a170503", null ],
      [ "SetCommFeature", "struct_u_s_b_d___c_d_c___i_n_i_t___p_a_r_a_m.html#a2aa7ba43120b9063a40baed237e54e2a", null ],
      [ "SetCtrlLineState", "struct_u_s_b_d___c_d_c___i_n_i_t___p_a_r_a_m.html#a697915926d68a3c5623c49d665c6ed24", null ],
      [ "SetLineCode", "struct_u_s_b_d___c_d_c___i_n_i_t___p_a_r_a_m.html#a4fd7145730297309cd600731b292943f", null ]
    ] ],
    [ "USBD_CDC_API", "struct_u_s_b_d___c_d_c___a_p_i.html", [
      [ "GetMemSize", "struct_u_s_b_d___c_d_c___a_p_i.html#a66487e730c2a8648196f23f475067cdf", null ],
      [ "init", "struct_u_s_b_d___c_d_c___a_p_i.html#a28d4ba7d5bec1b3cbccf7f5e60636338", null ],
      [ "SendNotification", "struct_u_s_b_d___c_d_c___a_p_i.html#a7d3d0a863f5ab96448c1247e91c2e116", null ]
    ] ],
    [ "USBD_CDC_API_T", "group___u_s_b_d___c_d_c.html#ga6b0174f3af699b4046e6ed721850c986", null ],
    [ "USBD_CDC_INIT_PARAM_T", "group___u_s_b_d___c_d_c.html#gaa55a5e1ad269a10a322d48be25c1926a", null ]
];