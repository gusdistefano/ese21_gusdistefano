var switch_8c =
[
    [ "LOW_EDGE", "switch_8c.html#a903810ee8215c7efccc9eaf458fa5772", null ],
    [ "N_SW", "switch_8c.html#af70f4bba30eb9c9f5918afca90f7ede1", null ],
    [ "Switch1Int", "switch_8c.html#a2e74731a30702d3acc7c0b0f7f41395f", null ],
    [ "Switch2Int", "switch_8c.html#a0ad21f1345dc6349ca5e99d081534d91", null ],
    [ "Switch3Int", "switch_8c.html#aab31211525294a15a34329d7209b9f82", null ],
    [ "Switch4Int", "switch_8c.html#a1f3c7d2e8c5c4d0523c32c21c9715670", null ],
    [ "SwitchActivInt", "group___switch.html#ga582967a1a4ca77d5c080b75c5db90cfb", null ],
    [ "SwitchesActivGroupInt", "group___switch.html#ga90d4d8426d55f0afa1788b27a71b29c9", null ],
    [ "SwitchesInit", "group___switch.html#ga8eb2865a73bf2d2b9fd6760958a0cc3c", null ],
    [ "SwitchesRead", "group___switch.html#gae2e64bab117e3f8ffc08ecae51f5e262", null ],
    [ "ptr_tec_group_int_func", "switch_8c.html#a190e41ef86b74a4235ac9b92f64dcacf", null ],
    [ "ptr_tec_int_func", "switch_8c.html#a0810f768552c94f87725b9438501a73d", null ]
];