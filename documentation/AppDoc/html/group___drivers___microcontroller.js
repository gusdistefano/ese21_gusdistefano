var group___drivers___microcontroller =
[
    [ "GPIO", "group___g_i_o_p.html", "group___g_i_o_p" ],
    [ "SPI", "group___s_p_i.html", "group___s_p_i" ],
    [ "Systemclock", "group___systemclock.html", "group___systemclock" ],
    [ "Timer", "group___timer.html", "group___timer" ],
    [ "UART", "group___u_a_r_t.html", "group___u_a_r_t" ]
];