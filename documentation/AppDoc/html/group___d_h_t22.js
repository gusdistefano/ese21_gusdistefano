var group___d_h_t22 =
[
    [ "dht22", "structdht22.html", [
      [ "gpioPin", "structdht22.html#aac0719041a981c2239101770e56567d6", null ],
      [ "gpioPort", "structdht22.html#af3118cdc8c5891b2284ddf62a43b12c6", null ],
      [ "scupin", "structdht22.html#a3ae94d112edddb3fa897d34f6583965a", null ],
      [ "scuport", "structdht22.html#a3d2da2ce9677bb374f379afafcc072b4", null ]
    ] ],
    [ "DHT22_Init", "group___d_h_t22.html#gaa869764938b2f5503c7e4e3816558163", null ],
    [ "DHT22_Read", "group___d_h_t22.html#gad9887e9b6e06f7f43183739f85de924f", null ]
];