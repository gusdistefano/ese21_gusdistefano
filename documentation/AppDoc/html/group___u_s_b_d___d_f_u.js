var group___u_s_b_d___d_f_u =
[
    [ "USBD_DFU_INIT_PARAM", "struct_u_s_b_d___d_f_u___i_n_i_t___p_a_r_a_m.html", [
      [ "DFU_Detach", "struct_u_s_b_d___d_f_u___i_n_i_t___p_a_r_a_m.html#a9cfa6c057e6d7a3332157e3765a0dc30", null ],
      [ "DFU_Done", "struct_u_s_b_d___d_f_u___i_n_i_t___p_a_r_a_m.html#a9088344133b748bc662bd44deafcdc1d", null ],
      [ "DFU_Ep0_Hdlr", "struct_u_s_b_d___d_f_u___i_n_i_t___p_a_r_a_m.html#adee1ad8088e5d153ffbb95202eb5631e", null ],
      [ "DFU_Read", "struct_u_s_b_d___d_f_u___i_n_i_t___p_a_r_a_m.html#a57fc57f77acf70cf49a38e6c1f925392", null ],
      [ "DFU_Write", "struct_u_s_b_d___d_f_u___i_n_i_t___p_a_r_a_m.html#a81e2aaf64b19178bd627f530426b6335", null ],
      [ "intf_desc", "struct_u_s_b_d___d_f_u___i_n_i_t___p_a_r_a_m.html#a4333b8418a39b34b08fb044640a10fae", null ],
      [ "mem_base", "struct_u_s_b_d___d_f_u___i_n_i_t___p_a_r_a_m.html#a5ba2f6f78705b63f0788f9b85f7f104c", null ],
      [ "mem_size", "struct_u_s_b_d___d_f_u___i_n_i_t___p_a_r_a_m.html#a3f6a3b0b58cbdacf4ce9a0c9b3d64794", null ],
      [ "pad", "struct_u_s_b_d___d_f_u___i_n_i_t___p_a_r_a_m.html#a29720fa149448a5be70ff846b72c6dde", null ],
      [ "wTransferSize", "struct_u_s_b_d___d_f_u___i_n_i_t___p_a_r_a_m.html#abf7c6492df9b515329616580afb383d2", null ]
    ] ],
    [ "USBD_DFU_API", "struct_u_s_b_d___d_f_u___a_p_i.html", [
      [ "GetMemSize", "struct_u_s_b_d___d_f_u___a_p_i.html#a048df985da6f33c0a5c911b50c9f8e75", null ],
      [ "init", "struct_u_s_b_d___d_f_u___a_p_i.html#aee4ae3f6c361340cbb7affe2d29d69e2", null ]
    ] ],
    [ "USBD_DFU_API_T", "group___u_s_b_d___d_f_u.html#ga33d37ef9e88ade26a6dffb0360ebbcb2", null ],
    [ "USBD_DFU_INIT_PARAM_T", "group___u_s_b_d___d_f_u.html#gac6d9058714e1c6138caac79bf3d7f605", null ]
];