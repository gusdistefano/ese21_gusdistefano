var group___d_h_t22___sensor =
[
    [ "dht22", "structdht22.html", [
      [ "gpioPin", "structdht22.html#aac0719041a981c2239101770e56567d6", null ],
      [ "gpioPort", "structdht22.html#af3118cdc8c5891b2284ddf62a43b12c6", null ],
      [ "scuPin", "structdht22.html#a8f1eb0e9c2cacf778879d25c8567d161", null ],
      [ "scuPort", "structdht22.html#a92e4ab7850d93d5713811a0f9cca522f", null ]
    ] ],
    [ "DHT22_Init", "group___d_h_t22___sensor.html#gaa869764938b2f5503c7e4e3816558163", null ],
    [ "DHT22_Read", "group___d_h_t22___sensor.html#gad9887e9b6e06f7f43183739f85de924f", null ]
];