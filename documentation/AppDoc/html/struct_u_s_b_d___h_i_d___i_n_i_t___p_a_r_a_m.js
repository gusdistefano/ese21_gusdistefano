var struct_u_s_b_d___h_i_d___i_n_i_t___p_a_r_a_m =
[
    [ "HID_Ep0_Hdlr", "struct_u_s_b_d___h_i_d___i_n_i_t___p_a_r_a_m.html#acaadefeddc44d74759afb300590d89a8", null ],
    [ "HID_EpIn_Hdlr", "struct_u_s_b_d___h_i_d___i_n_i_t___p_a_r_a_m.html#a8605d318b383c2b86c1f11850ff1e66e", null ],
    [ "HID_EpOut_Hdlr", "struct_u_s_b_d___h_i_d___i_n_i_t___p_a_r_a_m.html#ace22f3e0a209214d3eb7ff9e17d4ef7e", null ],
    [ "HID_GetPhysDesc", "struct_u_s_b_d___h_i_d___i_n_i_t___p_a_r_a_m.html#aa099eaf4a72a409cb532af20e1c00f27", null ],
    [ "HID_GetReport", "struct_u_s_b_d___h_i_d___i_n_i_t___p_a_r_a_m.html#a6b84d1c7f65126e79d6d46f4767bc2ae", null ],
    [ "HID_GetReportDesc", "struct_u_s_b_d___h_i_d___i_n_i_t___p_a_r_a_m.html#ab740f340d71da4d1f465ee796986a5ff", null ],
    [ "HID_SetIdle", "struct_u_s_b_d___h_i_d___i_n_i_t___p_a_r_a_m.html#a19f3b4dca18d277997dff6de89760796", null ],
    [ "HID_SetProtocol", "struct_u_s_b_d___h_i_d___i_n_i_t___p_a_r_a_m.html#a4808e0ffeb33aa723ec27a1887a3149e", null ],
    [ "HID_SetReport", "struct_u_s_b_d___h_i_d___i_n_i_t___p_a_r_a_m.html#aa6513f8776615e924f9942d0a703dfd3", null ],
    [ "intf_desc", "struct_u_s_b_d___h_i_d___i_n_i_t___p_a_r_a_m.html#a4333b8418a39b34b08fb044640a10fae", null ],
    [ "max_reports", "struct_u_s_b_d___h_i_d___i_n_i_t___p_a_r_a_m.html#a53f3f23a4e8d8f55c6ba182db636e0c9", null ],
    [ "mem_base", "struct_u_s_b_d___h_i_d___i_n_i_t___p_a_r_a_m.html#a5ba2f6f78705b63f0788f9b85f7f104c", null ],
    [ "mem_size", "struct_u_s_b_d___h_i_d___i_n_i_t___p_a_r_a_m.html#a3f6a3b0b58cbdacf4ce9a0c9b3d64794", null ],
    [ "pad", "struct_u_s_b_d___h_i_d___i_n_i_t___p_a_r_a_m.html#afe23e4c59de249880511510ea325a1ad", null ],
    [ "report_data", "struct_u_s_b_d___h_i_d___i_n_i_t___p_a_r_a_m.html#ac3148bb30684d21f9c0c5de7ff07220e", null ]
];