var searchData=
[
  ['readadcval_6808',['readAdcVal',['../adc__18xx__43xx_8c.html#a8edc226a08b05b47c994049bcd00c60c',1,'adc_18xx_43xx.c']]],
  ['reset_6809',['reset',['../enet__18xx__43xx_8c.html#aeb253604a11400185c3a9933e18c68c3',1,'enet_18xx_43xx.c']]],
  ['resetisr_6810',['ResetISR',['../cr__startup__lpc43xx_8c.html#a516ff8924be921fa3a1bb7754b1f5734',1,'cr_startup_lpc43xx.c']]],
  ['ringbuffer_5fflush_6811',['RingBuffer_Flush',['../group___ring___buffer.html#ga5f66a5dd980ef03877cf8e0c96ad4ebb',1,'ring_buffer.h']]],
  ['ringbuffer_5fgetcount_6812',['RingBuffer_GetCount',['../group___ring___buffer.html#ga7b69777c35694637acaf39e6bfcc1822',1,'ring_buffer.h']]],
  ['ringbuffer_5fgetfree_6813',['RingBuffer_GetFree',['../group___ring___buffer.html#ga75424687def8979742338366d39c8559',1,'ring_buffer.h']]],
  ['ringbuffer_5fgetsize_6814',['RingBuffer_GetSize',['../group___ring___buffer.html#ga2fc4b40b03afb19c8ea942da3cf3faf1',1,'ring_buffer.h']]],
  ['ringbuffer_5finit_6815',['RingBuffer_Init',['../group___ring___buffer.html#gaaf3bb51f2228ea1bea603e19c7eba5bb',1,'RingBuffer_Init(RINGBUFF_T *RingBuff, void *buffer, int itemSize, int count):&#160;ring_buffer.c'],['../group___ring___buffer.html#gaaf3bb51f2228ea1bea603e19c7eba5bb',1,'RingBuffer_Init(RINGBUFF_T *RingBuff, void *buffer, int itemSize, int count):&#160;ring_buffer.c']]],
  ['ringbuffer_5finsert_6816',['RingBuffer_Insert',['../group___ring___buffer.html#gaafdee54f2525b2c7a983d1a631b42226',1,'RingBuffer_Insert(RINGBUFF_T *RingBuff, const void *data):&#160;ring_buffer.c'],['../group___ring___buffer.html#gaafdee54f2525b2c7a983d1a631b42226',1,'RingBuffer_Insert(RINGBUFF_T *RingBuff, const void *data):&#160;ring_buffer.c']]],
  ['ringbuffer_5finsertmult_6817',['RingBuffer_InsertMult',['../group___ring___buffer.html#gafeafb521d4e03052ab2c893fd0e388d5',1,'RingBuffer_InsertMult(RINGBUFF_T *RingBuff, const void *data, int num):&#160;ring_buffer.c'],['../group___ring___buffer.html#gafeafb521d4e03052ab2c893fd0e388d5',1,'RingBuffer_InsertMult(RINGBUFF_T *RingBuff, const void *data, int num):&#160;ring_buffer.c']]],
  ['ringbuffer_5fisempty_6818',['RingBuffer_IsEmpty',['../group___ring___buffer.html#ga6f03e04a69262864bde4f35fc6f3dfb5',1,'ring_buffer.h']]],
  ['ringbuffer_5fisfull_6819',['RingBuffer_IsFull',['../group___ring___buffer.html#ga760da012435262add1d8d7aa79e873a0',1,'ring_buffer.h']]],
  ['ringbuffer_5fpop_6820',['RingBuffer_Pop',['../group___ring___buffer.html#gaf3ce7f43677c2b4c6eedb3cc4962b80d',1,'RingBuffer_Pop(RINGBUFF_T *RingBuff, void *data):&#160;ring_buffer.c'],['../group___ring___buffer.html#gaf3ce7f43677c2b4c6eedb3cc4962b80d',1,'RingBuffer_Pop(RINGBUFF_T *RingBuff, void *data):&#160;ring_buffer.c']]],
  ['ringbuffer_5fpopmult_6821',['RingBuffer_PopMult',['../group___ring___buffer.html#gae0ef7bb96d1fe84ae1441b7c214b1e56',1,'RingBuffer_PopMult(RINGBUFF_T *RingBuff, void *data, int num):&#160;ring_buffer.c'],['../group___ring___buffer.html#gae0ef7bb96d1fe84ae1441b7c214b1e56',1,'RingBuffer_PopMult(RINGBUFF_T *RingBuff, void *data, int num):&#160;ring_buffer.c']]],
  ['rit_5firqhandler_6822',['RIT_IRQHandler',['../timer_8c.html#a94381ae71a78d9fcc2b50d0b13a5b45e',1,'timer.c']]]
];
