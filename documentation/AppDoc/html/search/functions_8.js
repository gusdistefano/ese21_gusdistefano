var searchData=
[
  ['i2cread_6775',['I2CRead',['../group___i2c.html#gad9f26ef217f84a9f6bc9945ee80fd418',1,'I2CRead(uint8_t i2cSlaveAddress, uint8_t *dataToReadBuffer, uint16_t dataToReadBufferSize, bool sendWriteStop, uint8_t *receiveDataBuffer, uint16_t receiveDataBufferSize, bool sendReadStop):&#160;i2c.c'],['../group___i2c.html#gad9f26ef217f84a9f6bc9945ee80fd418',1,'I2CRead(uint8_t i2cSlaveAddress, uint8_t *dataToReadBuffer, uint16_t dataToReadBufferSize, bool sendWriteStop, uint8_t *receiveDataBuffer, uint16_t receiveDataBufferSize, bool sendReadStop):&#160;i2c.c']]],
  ['i2cwrite_6776',['I2CWrite',['../group___i2c.html#ga6f94847c6b3a8107027b54039b5084be',1,'I2CWrite(uint8_t i2cSlaveAddress, uint8_t *transmitDataBuffer, uint16_t transmitDataBufferSize, bool sendWriteStop):&#160;i2c.c'],['../group___i2c.html#ga6f94847c6b3a8107027b54039b5084be',1,'I2CWrite(uint8_t i2cSlaveAddress, uint8_t *transmitDataBuffer, uint16_t transmitDataBufferSize, bool sendWriteStop):&#160;i2c.c']]],
  ['init_5fi2c_6777',['Init_I2c',['../group___i2c.html#ga4c36ed89a3ce4e05b04818aa875f1572',1,'Init_I2c(uint32_t clockRateHz):&#160;i2c.c'],['../group___i2c.html#ga4c36ed89a3ce4e05b04818aa875f1572',1,'Init_I2c(uint32_t clockRateHz):&#160;i2c.c']]],
  ['initdynmem_6778',['initDynMem',['../emc__18xx__43xx_8c.html#aaee0a1bdfbf4b5e3212f3ef8498afe4a',1,'emc_18xx_43xx.c']]],
  ['initstaticmem_6779',['initStaticMem',['../emc__18xx__43xx_8c.html#ac263a3e68931d182564bca53b499021b',1,'emc_18xx_43xx.c']]],
  ['isi2cbusfree_6780',['isI2CBusFree',['../i2c__18xx__43xx_8c.html#adf0dffdb74771c1f98a5e959e6dca498',1,'i2c_18xx_43xx.c']]],
  ['ismasterstate_6781',['isMasterState',['../i2c__18xx__43xx_8c.html#aa5d73e6cbd7622d475d0d050c68a6c4c',1,'i2c_18xx_43xx.c']]],
  ['isslaveaddrmatching_6782',['isSlaveAddrMatching',['../i2c__18xx__43xx_8c.html#a4c1168486165c01fd04e1d916fbd1219',1,'i2c_18xx_43xx.c']]],
  ['itm_5fcheckchar_6783',['ITM_CheckChar',['../group___c_m_s_i_s__core___debug_functions.html#gae61ce9ca5917735325cd93b0fb21dd29',1,'core_cm4.h']]],
  ['itm_5freceivechar_6784',['ITM_ReceiveChar',['../group___c_m_s_i_s__core___debug_functions.html#gac3ee2c30a1ac4ed34c8a866a17decd53',1,'core_cm4.h']]],
  ['itm_5fsendchar_6785',['ITM_SendChar',['../group___c_m_s_i_s__core___debug_functions.html#gac90a497bd64286b84552c2c553d3419e',1,'core_cm4.h']]]
];
