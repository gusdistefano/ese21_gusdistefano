var indexSectionsWithContent =
{
  0: "_abcdefghilmnopqrstuvwxz",
  1: "_acdeghilnoprstux",
  2: "acdefghilmopqrstuw",
  3: "_acdefghilmnprstu",
  4: "_abcdefghilmnopqrstuvwxz",
  5: "abcdhimprstuvw",
  6: "_bcdefghilrstu",
  7: "abcdefghilmnpqrstuw",
  8: "_abcdeghilmnoprstuw",
  9: "abcdfghilmnstu",
  10: "m"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "typedefs",
  6: "enums",
  7: "enumvalues",
  8: "defines",
  9: "groups",
  10: "pages"
};

var indexSectionLabels =
{
  0: "Todo",
  1: "Estructuras de Datos",
  2: "Archivos",
  3: "Funciones",
  4: "Variables",
  5: "typedefs",
  6: "Enumeraciones",
  7: "Valores de enumeraciones",
  8: "defines",
  9: "Grupos",
  10: "Páginas"
};

