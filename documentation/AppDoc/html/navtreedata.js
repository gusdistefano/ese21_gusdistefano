/*
@licstart  The following is the entire license notice for the
JavaScript code in this file.

Copyright (C) 1997-2019 by Dimitri van Heesch

This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

@licend  The above is the entire license notice
for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "Arquitectura y Programacion de Sistemas Embebidos", "index.html", [
    [ "MISRA-C:2004 Compliance Exceptions", "_c_m_s_i_s__m_i_s_r_a__exceptions.html", null ],
    [ "Módulos", "modules.html", "modules" ],
    [ "Estructuras de Datos", "annotated.html", [
      [ "Estructura de datos", "annotated.html", "annotated_dup" ],
      [ "Índice de estructura de datos", "classes.html", null ],
      [ "Campos de datos", "functions.html", [
        [ "Todo", "functions.html", "functions_dup" ],
        [ "Variables", "functions_vars.html", "functions_vars" ]
      ] ]
    ] ],
    [ "Archivos", "files.html", [
      [ "Lista de archivos", "files.html", "files_dup" ],
      [ "Globales", "globals.html", [
        [ "Todo", "globals.html", "globals_dup" ],
        [ "Funciones", "globals_func.html", "globals_func" ],
        [ "Variables", "globals_vars.html", null ],
        [ "typedefs", "globals_type.html", null ],
        [ "Enumeraciones", "globals_enum.html", null ],
        [ "Valores de enumeraciones", "globals_eval.html", "globals_eval" ],
        [ "defines", "globals_defs.html", "globals_defs" ]
      ] ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"_c_m_s_i_s__m_i_s_r_a__exceptions.html",
"dht22_8c.html#ada74e7db007a68e763f20c17f2985356",
"functions_vars_a.html",
"group___a_d_c__18_x_x__43_x_x.html#gga67b0a9ec8f34cf712b1f1c0119ba3d50a0db887b05fe2d35518140c53e4d3cd37",
"group___c_c_a_n__18_x_x__43_x_x.html#ga7227d00cb5d6ab0055f183f272ee647c",
"group___c_h_i_p___s_d_m_m_c___definitions.html#ga749e6ba0c8ccfeec094e92b2367c7a98",
"group___c_l_o_c_k__18_x_x__43_x_x.html#gabdd04536f16b3c7b588757d024e53da6",
"group___c_m_s_i_s___core___n_v_i_c_functions.html#ga332e10ef9605dc6eb10b9e14511930f8",
"group___c_m_s_i_s___d_w_t.html#gadd798deb0f1312feab4fb05dcddc229b",
"group___c_m_s_i_s___s_c_b.html#gabacedaefeefc73d666bbe59ece904493",
"group___c_m_s_i_s___t_p_i.html#gacba2edfc0499828019550141356b0dcb",
"group___c_m_s_i_s__core___debug_functions.html#ga7a1caf92f32fe9ebd8d1fe89b06c7776",
"group___c_m_s_i_s__core___debug_functions.html#gad987483f555c0f6034ef24a8840f337d",
"group___d_a_c__18_x_x__43_x_x.html#gabdfe0f0d61a206418a2ffdba26653873",
"group___e_m_c__18_x_x__43_x_x.html#gace9c2c7bc7470cb911f27bd4bb93e01c",
"group___e_n_e_t__18_x_x__43_x_x.html#ga7b925bfc1df703ef75a309840d62deeb",
"group___e_v_r_t__18_x_x__43_x_x.html#ga5320424173f844bb1484ae1523e3454a",
"group___g_p_d_m_a__18_x_x__43_x_x.html#ga67bb6ed286afe6d4091c0fcd8799b451",
"group___g_p_i_o__18_x_x__43_x_x.html#gad799db2a825ded50fc7998f228db1b24",
"group___i2_c__18_x_x__43_x_x.html#ga21aa839302786105dcf6a96be0e6e8bc",
"group___i2_s__18_x_x__43_x_x.html#ga0c7a808e84e2c0a3f7ffbba089c7d380",
"group___i_a_p__18_x_x__43_x_x.html#ga8600f9e930f1dee5d67a2bc11efcde63",
"group___l_p_c___types___public___types.html#ggaf5297347bed33665c55dd0e9c7840403ae00130e64382c35d172d226b79aa9acb",
"group___p_e_r_i_p_h__18_x_x___b_a_s_e.html#gaf34264b6322506295cc7b7e1ac903148",
"group___p_i_n_i_n_t__18_x_x__43_x_x.html#ga4a9efb559231903508b66858a43b552d",
"group___r_t_c__18_x_x__43_x_x.html#ga385935dbe73607b57dc1d31793b34f74",
"group___ring___buffer.html#gaf3ce7f43677c2b4c6eedb3cc4962b80d",
"group___s_c_u__18_x_x__43_x_x.html#gaf7f798c364e281b3aa3247516e0a913e",
"group___s_d_i_f__18_x_x__43_x_x.html#gadc6abfcb938e3ac834cfb0300d71c375",
"group___s_i_m800_l.html#ga574b28c4c71a589dec109520ed01acaf",
"group___s_s_p__18_x_x__43_x_x.html#gga2f99e08511788c146ae9b35023e4a61ca3d5cc36200365dc046d283f48711d885",
"group___t_i_m_e_r__18_x_x__43_x_x.html#gae9a2575f38b3acaf6255caf15c5b65df",
"group___u_a_r_t__18_x_x__43_x_x.html#ga6f78952aec5835ac753718323b681910",
"group___u_s_b_d___core.html#ga40131e4daec200a7d931c2b26e43d27f",
"group___u_s_b_d___h_i_d.html#ga29edc17f7621d8e8ae6784719a8fc0d3",
"group___u_s_b_d___h_i_d.html#gaaf5fe8bc99060dfc70ed7961592591eb",
"i2c__18xx__43xx_8c.html",
"struct___c_d_c___l_i_n_e___c_o_d_i_n_g.html#a47f5ab7fcc915dedd560214e61438bc3",
"struct_g_p_d_m_a___c_h___c_f_g___t.html#a0cb76a8204c8f81b9ac1ab3c8840c499",
"struct_l_p_c___e_m_c___t.html#a88ed239d4cca54a191bf12fb97cdb198",
"struct_l_p_c___o_t_p___t.html#aed395d3b6adea1f7a85a2ccf2d0b87e7",
"struct_l_p_c___u_s_a_r_t___t.html#ad2368f94732ad41da998cb2eb03cd2e0",
"struct_u_s_b_d___m_s_c___i_n_i_t___p_a_r_a_m.html#aabec868bf4472f00a72dccebaafce27c",
"usbd__adc_8h.html#a81d32b3e852cc23421681c2bd2fabd27",
"usbd__dfu_8h.html#a3f025bde53c777c5e0493ef54bbeb143a07e74bf2d38282e7bdd7d3914bf912f2"
];

var SYNCONMSG = 'click en deshabilitar sincronización';
var SYNCOFFMSG = 'click en habilitar sincronización';