var group___analog___i_o =
[
    [ "analog_input_config", "structanalog__input__config.html", [
      [ "input", "structanalog__input__config.html#a2475c78015c7393c1c9939ab5a57ad9f", null ],
      [ "mode", "structanalog__input__config.html#a37e90f5e3bd99fac2021fb3a326607d4", null ],
      [ "pAnalogInput", "structanalog__input__config.html#a2298b64ab1835d56f927cb94137f36ad", null ]
    ] ],
    [ "AINPUTS_BURST_READ", "group___analog___i_o.html#ga9439488d01a948fab1b2f8bc0bab00fa", null ],
    [ "AINPUTS_SINGLE_READ", "group___analog___i_o.html#ga5b01b5f293bd90a7bf9f4a988b3fce04", null ],
    [ "ANALOG_INPUT_READ", "group___analog___i_o.html#ga7b3118011a8b70756613058be97884ab", null ],
    [ "ANALOG_OUTPUT_RANGE", "group___analog___i_o.html#ga950c25493cc190f761c14db95326beee", null ],
    [ "ANALOG_OUTPUT_WROTE", "group___analog___i_o.html#ga731c5701b891f35c7f95766fdda7aa9b", null ],
    [ "CH0", "group___analog___i_o.html#gaf6f592bd18a57b35061f111d32a7f637", null ],
    [ "CH1", "group___analog___i_o.html#ga90643238cf4f49aae5f0ab215ada7889", null ],
    [ "CH2", "group___analog___i_o.html#ga92dc73af14a6902eadd21bdee033cbfb", null ],
    [ "CH3", "group___analog___i_o.html#ga9a593b4f2e9cc1ab563a99ccc361ac2e", null ],
    [ "DAC", "group___analog___i_o.html#ga4aa2a4ab86ce00c23035e5cee2e7fc7e", null ],
    [ "ERROR_CHANNEL_SELECTION", "group___analog___i_o.html#gac826d6653b6cb9c76e25f9b767f6eafa", null ],
    [ "AnalogInputInit", "group___analog___i_o.html#ga75e49898ce3cf18d67e4d463c6e2a8de", null ],
    [ "AnalogInputRead", "group___analog___i_o.html#gad13e6436e0177f0e17dc1a01fc4d47af", null ],
    [ "AnalogInputReadPolling", "group___analog___i_o.html#ga1673192453e03e9fab4976534b20f3e9", null ],
    [ "AnalogOutputInit", "group___analog___i_o.html#gab57399e946247652a096a0e2d3a1b69a", null ],
    [ "AnalogOutputWrite", "group___analog___i_o.html#ga464364f0790a3e7d1ab9d9e9c2d9092c", null ],
    [ "AnalogStartConvertion", "group___analog___i_o.html#ga551f68a70593d1b97e8c41a470707368", null ]
];