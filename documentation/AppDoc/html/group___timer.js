var group___timer =
[
    [ "timer_config", "structtimer__config.html", [
      [ "period", "structtimer__config.html#a258408d6d5d13a24bfa5211d81ce1682", null ],
      [ "pFunc", "structtimer__config.html#a9cead290357aaae808d4db4ab87784df", null ],
      [ "timer", "structtimer__config.html#a0ff73b1428e9cd11a4f7a6cf46b7b550", null ]
    ] ],
    [ "lpc4337", "group___timer.html#gadfc13aced9eecd5bf67ab539639ef200", null ],
    [ "mk60fx512vlq15", "group___timer.html#gac5996bc3bcae001e239c5563704c0d7d", null ],
    [ "TIMER_A", "group___timer.html#ga47d0eac91b49052bcd964223eb8eeffc", null ],
    [ "TIMER_A_1ms_TICK", "group___timer.html#ga162d13fe964cde50a95d80a88349be7d", null ],
    [ "TIMER_B", "group___timer.html#ga394289ce488144cf553f2193ad4f38c3", null ],
    [ "TIMER_B_1ms_TICK", "group___timer.html#gaef8c2dd280d8e80f0613ebf320eb2838", null ],
    [ "TIMER_C", "group___timer.html#ga7f80ae7115920d2e2c6e57670471a216", null ],
    [ "TIMER_C_1us_TICK", "group___timer.html#ga32cf8edb66a6be9668cbeaea51cfabcf", null ],
    [ "TimerInit", "group___timer.html#ga148b01475111265d1798f5c204a93df0", null ],
    [ "TimerReset", "group___timer.html#ga479d496a6ad7a733fb8da2f36800b76b", null ],
    [ "TimerStart", "group___timer.html#ga31487bffd934ce838a72f095f9231b24", null ],
    [ "TimerStop", "group___timer.html#gab652b899be3054eae4649a9063ec904b", null ]
];