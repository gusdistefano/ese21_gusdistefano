var struct_o_t_p___a_p_i___t =
[
    [ "GenRand", "struct_o_t_p___a_p_i___t.html#ae4c9d4a64802d4da1b014844fb0bec9d", null ],
    [ "Init", "struct_o_t_p___a_p_i___t.html#a5e7dc82fcbb5a30770356dc4ad75e425", null ],
    [ "ProgBootSrc", "struct_o_t_p___a_p_i___t.html#aaec301fc73262c70512859fd1bdc033a", null ],
    [ "ProgGP0", "struct_o_t_p___a_p_i___t.html#a6cd8cc47e64add0ff2fec071c84a8401", null ],
    [ "ProgGP1", "struct_o_t_p___a_p_i___t.html#ab48de58329f0bcf881988ded0a3bf394", null ],
    [ "ProgGP2", "struct_o_t_p___a_p_i___t.html#a77d5828989110682317e9eb839c6fe9a", null ],
    [ "ProgJTAGDis", "struct_o_t_p___a_p_i___t.html#ad4430780b29a4c8230c066f37d4481df", null ],
    [ "ProgKey1", "struct_o_t_p___a_p_i___t.html#a23dce15a3848814f8dda8967b73623f8", null ],
    [ "ProgKey2", "struct_o_t_p___a_p_i___t.html#a19c6f0b1142e43ae3f8322ad87b1241f", null ],
    [ "ProgUSBID", "struct_o_t_p___a_p_i___t.html#a89f602663458240c58a5d95dee03fb66", null ],
    [ "reserved01", "struct_o_t_p___a_p_i___t.html#a8d5004536bec0ff0e679112ce554af6c", null ],
    [ "reserved02", "struct_o_t_p___a_p_i___t.html#a5092df8b6837fdfef7763c16671c7950", null ],
    [ "reserved03", "struct_o_t_p___a_p_i___t.html#aea1729fb238d8d5d4f33e50303c67d4e", null ],
    [ "reserved04", "struct_o_t_p___a_p_i___t.html#aef772adb3cdb4d8979ba092b71d8ba67", null ]
];