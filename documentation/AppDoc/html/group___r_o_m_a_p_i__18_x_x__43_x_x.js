var group___r_o_m_a_p_i__18_x_x__43_x_x =
[
    [ "OTP_API_T", "struct_o_t_p___a_p_i___t.html", [
      [ "GenRand", "struct_o_t_p___a_p_i___t.html#ae4c9d4a64802d4da1b014844fb0bec9d", null ],
      [ "Init", "struct_o_t_p___a_p_i___t.html#a5e7dc82fcbb5a30770356dc4ad75e425", null ],
      [ "ProgBootSrc", "struct_o_t_p___a_p_i___t.html#aaec301fc73262c70512859fd1bdc033a", null ],
      [ "ProgGP0", "struct_o_t_p___a_p_i___t.html#a6cd8cc47e64add0ff2fec071c84a8401", null ],
      [ "ProgGP1", "struct_o_t_p___a_p_i___t.html#ab48de58329f0bcf881988ded0a3bf394", null ],
      [ "ProgGP2", "struct_o_t_p___a_p_i___t.html#a77d5828989110682317e9eb839c6fe9a", null ],
      [ "ProgJTAGDis", "struct_o_t_p___a_p_i___t.html#ad4430780b29a4c8230c066f37d4481df", null ],
      [ "ProgKey1", "struct_o_t_p___a_p_i___t.html#a23dce15a3848814f8dda8967b73623f8", null ],
      [ "ProgKey2", "struct_o_t_p___a_p_i___t.html#a19c6f0b1142e43ae3f8322ad87b1241f", null ],
      [ "ProgUSBID", "struct_o_t_p___a_p_i___t.html#a89f602663458240c58a5d95dee03fb66", null ],
      [ "reserved01", "struct_o_t_p___a_p_i___t.html#a8d5004536bec0ff0e679112ce554af6c", null ],
      [ "reserved02", "struct_o_t_p___a_p_i___t.html#a5092df8b6837fdfef7763c16671c7950", null ],
      [ "reserved03", "struct_o_t_p___a_p_i___t.html#aea1729fb238d8d5d4f33e50303c67d4e", null ],
      [ "reserved04", "struct_o_t_p___a_p_i___t.html#aef772adb3cdb4d8979ba092b71d8ba67", null ]
    ] ],
    [ "AES_API_T", "struct_a_e_s___a_p_i___t.html", [
      [ "Init", "struct_a_e_s___a_p_i___t.html#a5e7dc82fcbb5a30770356dc4ad75e425", null ],
      [ "LoadIV_IC", "struct_a_e_s___a_p_i___t.html#a8d1366653c6c4cfc7e46fad446704a51", null ],
      [ "LoadIV_SW", "struct_a_e_s___a_p_i___t.html#a226333256f1de75865f80ac75dff7764", null ],
      [ "LoadKey1", "struct_a_e_s___a_p_i___t.html#aca5ad188d8694f82551d53f8d719c708", null ],
      [ "LoadKey2", "struct_a_e_s___a_p_i___t.html#a28abfe87795ec47e6e3cb9fcb22d1467", null ],
      [ "LoadKeyRNG", "struct_a_e_s___a_p_i___t.html#a4ce2623a4f10ab06bf28801f985e5fc4", null ],
      [ "LoadKeySW", "struct_a_e_s___a_p_i___t.html#a486d98466f5d0922732a81f72abaef50", null ],
      [ "Operate", "struct_a_e_s___a_p_i___t.html#ac84b67159f02079f0f665413ed5890a3", null ],
      [ "ProgramKey1", "struct_a_e_s___a_p_i___t.html#aad2686244b6c5610df9f43e85d1d0609", null ],
      [ "ProgramKey2", "struct_a_e_s___a_p_i___t.html#a75afcbe341cc6fd3dbb7bc0bcd33744c", null ],
      [ "SetMode", "struct_a_e_s___a_p_i___t.html#a232a1b166e253abcfb8f1b99bcf72bad", null ]
    ] ],
    [ "LPC_ROM_API_T", "struct_l_p_c___r_o_m___a_p_i___t.html", [
      [ "endMarker", "struct_l_p_c___r_o_m___a_p_i___t.html#a7f34eaacc2cb2f0b7d2ecf6118de8571", null ],
      [ "iap_entry", "struct_l_p_c___r_o_m___a_p_i___t.html#a3ca6b3a27f4e23515d255365dcb90a15", null ],
      [ "pAes", "struct_l_p_c___r_o_m___a_p_i___t.html#adeb986ef29cf8092282c1ea911defaa0", null ],
      [ "pOtp", "struct_l_p_c___r_o_m___a_p_i___t.html#a86471f8c6688aac6e7445731c93f1e56", null ],
      [ "reserved", "struct_l_p_c___r_o_m___a_p_i___t.html#aa6a7e7637e402ba46cf05af64354b460", null ],
      [ "spifiApiBase", "struct_l_p_c___r_o_m___a_p_i___t.html#a89fd951104ec1dfacddd83e15df96c9b", null ],
      [ "usbdApiBase", "struct_l_p_c___r_o_m___a_p_i___t.html#a283da22255e47638afbbb7e8e0cef150", null ]
    ] ],
    [ "IAP_ENTRY_LOCATION", "group___r_o_m_a_p_i__18_x_x__43_x_x.html#ga3d74da11ceb6b4ccf6d3730825406515", null ],
    [ "IAP_FLASH_BANK_A", "group___r_o_m_a_p_i__18_x_x__43_x_x.html#gafb9244f2e5a4884eb1bf5fb89c2b7f5b", null ],
    [ "IAP_FLASH_BANK_B", "group___r_o_m_a_p_i__18_x_x__43_x_x.html#ga1000a64e320d9e09ba9c671bbb380dd2", null ],
    [ "LPC_ROM_API", "group___r_o_m_a_p_i__18_x_x__43_x_x.html#ga93f07fc38c09c20a7141e175ce599ef7", null ],
    [ "LPC_ROM_API_BASE_LOC", "group___r_o_m_a_p_i__18_x_x__43_x_x.html#gafd3ca6cb0ad8c0c0b5be437a8387e51b", null ]
];